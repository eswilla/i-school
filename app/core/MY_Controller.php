<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    public $data = array();

    public function __construct() {
	parent::__construct();
	$this->data['errors'] = array();
    }

    public function user_agent() {
	$this->load->library('user_agent');
	if ($this->agent->is_browser()) {
	    $agent = $this->agent->browser() . ' ' . $this->agent->version();
	} elseif ($this->agent->is_robot()) {
	    $agent = $this->agent->robot();
	} elseif ($this->agent->is_mobile()) {
	    $agent = $this->agent->mobile();
	} else {
	    $agent = 'Unidentified User Agent';
	}
	$data = array(
	    'agent' => $agent,
	    'platform' => $this->agent->platform()
	);
	return (object) $data;
    }

}

/* End of file MY_Controller.php */
/* Location: .//D/xampp/htdocs/school/mvc/core/MY_Controller.php */