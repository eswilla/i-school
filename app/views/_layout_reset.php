<?php echo doctype("html5"); ?>
<html class="white-bg-login" lang="en">
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<title>Sign in</title>

	<!-- bootstrap 3.0.2 -->
	<link href="<?php echo base_url('assets/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet"  type="text/css">
	<!-- font Awesome -->
	<link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet"  type="text/css">
	<!-- Style -->
	<!--<link href="<?php echo base_url('assets/inilabs/style.css'); ?>" rel="stylesheet"  type="text/css">-->
	<link href="<?php echo base_url('assets/theme_two/styles/main.css'); ?>" rel="stylesheet"  type="text/css">
	<link href="<?php echo base_url('assets/inilabs/inilabs.css'); ?>" rel="stylesheet"  type="text/css">
	<link href="<?php echo base_url('assets/inilabs/responsive.css'); ?>" rel="stylesheet"  type="text/css">
    </head>

    <body class="white-bg-login login-block">
	
	
	<div class="page-form">
	    <div class="panel panel-blue">
		<div class="panel-body">

		    <div class="form-body login-padding">
			<div class="col-md-12 text-center">
			    <h1>
			<center><?php echo $siteinfos->sname; ?></center></h1>
			
			</div>
			<div class="form-group">
			    <div class="col-md-3">
				<?php
//				if (count($siteinfos->photo)) {
//				    echo "<img class='img-responsive' style='margin-top: -10px; border:2px #e5e5e5 solid' width='50' height='50' src=" . base_url('uploads/images/' . $siteinfos->photo) . " />";
//				}
				?>
			    </div>
			    <div class="col-md-9">
				
			    </div>
			</div>
			<?php $this->load->view($subview); ?>
		    </div>

		</div>
	    </div>
	</div>

	
	
	<script type="text/javascript" src="<?php echo base_url('assets/inilabs/jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/bootstrap.min.js'); ?>"></script>
    </body>
</html>