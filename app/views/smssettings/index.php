
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-wrench"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_smssettings') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="col-sm-12">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="<?php if ($clickatell == 1) echo 'active'; ?>"><a data-toggle="tab" href="#clickatell" aria-expanded="true">karibuSMS</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="clickatell" class="tab-pane <?php if ($clickatell == 1) echo 'active'; ?> ">
                                <br>
                                <div class="row">
                                    <div class="col-sm-8">
					<div  class="alert alert-info"><a href="http://www.karibusms.com/api" target="_blank">www.karibusms.com/api</a></div>
                                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
					    <?php echo form_hidden('type', 'clickatell'); ?> 
					    <?php
					    if (form_error('clickatell_username'))
						echo "<div class='form-group has-error' >";
					    else
						echo "<div class='form-group' >";
					    ?>
					    <label for="clickatell_username" class="col-sm-2 control-label">
						<?= $this->lang->line("smssettings_username") ?>
					    </label>
					    <div class="col-sm-6">
						<input type="text" class="form-control" id="clickatell_username" name="clickatell_username" value="<?= set_value('clickatell_username', $set_clickatell['clickatell_username']) ?>" >
					    </div>
					    <span class="col-sm-4 control-label">
						<?php echo form_error('clickatell_username'); ?>
					    </span>
				    </div>

				    <?php
				    if (form_error('clickatell_api_key'))
					echo "<div class='form-group has-error' >";
				    else
					echo "<div class='form-group' >";
				    ?>
				    <label for="clickatell_api_key" class="col-sm-2 control-label">
					<?= $this->lang->line("smssettings_api_key") ?>
				    </label>
				    <div class="col-sm-6">
					<input type="text" class="form-control" id="clickatell_api_key" name="clickatell_api_key" value="<?= set_value('clickatell_api_key', $set_clickatell['clickatell_api_key']) ?>" >
				    </div>
				    <span class="col-sm-4 control-label">
					<?php echo form_error('clickatell_api_key'); ?>
				    </span>
				</div>

				<?php
				if (form_error('htype'))
				    echo "<div class='form-group has-error' >";
				else
				    echo "<div class='form-group' >";
				?>
				<label for="htype" class="col-sm-2 control-label">
				    <?= $this->lang->line("smssettings_type")  ?>
				</label>
				<div class="col-sm-6">
				    <?php
				    $array = array(
					0 => $this->lang->line('smssettings_select_type'),
					$this->lang->line('smssettings_smartphone') => $this->lang->line('smssettings_smartphone'),
					$this->lang->line('smssettings_internetsms') => $this->lang->line('smssettings_internetsms')
				    );
				    echo form_dropdown("smstype", $array, set_value("smstype"), "id='smstype' class='form-control'");
				    ?>
				</div>
				<span class="col-sm-4 control-label">
				    <?php echo form_error('smstype'); ?>
				</span>
			    </div>

			    <div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
				    <input type="submit" class="btn btn-success" value="<?= $this->lang->line("save") ?>" >
				</div>
			    </div>
			    </form>
			</div>
		    </div>
		</div>

	    </div>
	</div> <!-- nav-tabs-custom -->
    </div>


</div> <!-- col-sm-12 -->

</div><!-- row -->
</div><!-- Body -->
</div><!-- /.box -->