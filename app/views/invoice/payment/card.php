<div class="box">
    <!-- /.box-header -->
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("invoice/index") ?>"><?= $this->lang->line('menu_invoice') ?></a></li>
            <li class="active"><?= $this->lang->line('add_payment') ?></li>
        </ol>
    </div>
    <?php
    define('EXCHANGE_RATE', 2000); //this is just approximation
    ?>

    <div class="box-body">
        <div class="row">

	    <div class="row">
		<div class="col-md-6">
		    <h4>Online payment with Debit or Credit Card</h4>
		    <p align="center"><?= img(base_url('uploads/images/payments.png')) ?></p>
		    <br/>
		    <form id="myCCForm" class="form-horizontal" action="<?= base_url() ?>payment/process_twocheckout" method="POST">
			<!-- The $10 amount is set on the server side -->


                        <div class='form-group' >

			    <label for="cardno" class="col-sm-4 control-label" style="text-align: right;" >
				Credit Card Number:
			    </label>
			    <div class="col-sm-6">
				 <input id="ccNo"  type="text" maxlength="20" autocomplete="off" value="" autofocus />
			    </div>
			</div>

			
			<div class='form-group' >

			    <label for="cvs" class="col-sm-4 control-label" style="text-align: right;">
				CVC:
			    </label>
			    <div class="col-sm-6">
				 <input id="cvv" type="text" maxlength="4" autocomplete="off" value=""/>
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="col-md-4 control-label" style="text-align: right;">Expiry Date: </label>
			    <div class="col-sm-6">
			    <select id="expMonth">
				<option value="01">Jan</option>
				<option value="02">Feb</option>
				<option value="03">Mar</option>
				<option value="04">Apr</option>
				<option value="05">May</option>
				<option value="06">Jun</option>
				<option value="07">Jul</option>
				<option value="08">Aug</option>
				<option value="09">Sep</option>
				<option value="10">Oct</option>
				<option value="11">Nov</option>
				<option value="12">Dec</option>
			    </select>
			    <select id="expYear">
				<option value="13">2013</option>
				<option value="14">2014</option>
				<option value="15">2015</option>
				<option value="16">2016</option>
				<option value="17">2017</option>
				<option value="18">2018</option>
				<option value="19">2019</option>
				<option value="20">2020</option>
				<option value="21">2021</option>
				<option value="22">2022</option>
			    </select>
			    </div>
			</div>
			<label class="col-md-4"></label>
			<button id="process-payment-btn" class="btn btn-primary"  type="button">Process Payment</button>
		    </form>
		    <div id="span_loader"></div>
		</div>
		<div class="col-md-6">
		    <h4 class="heading">Payment Summary</h4>
		    <table id="user" class="table table-bordered table-striped" style="clear: both">
			<tbody>
			    <tr>
				<td class="column-left">Payment Amount:</td>
				<td class="column-right">

				    <?= $amountUSD = round($amount / EXCHANGE_RATE, 2); ?> USD approx
				</td>
			    </tr>
			    <tr>
				<td>Service Charge</td>
				<td>
				    <?= $service_charge = 0.05 * $amountUSD + 0.47; ?> approx
				</td>
			    </tr>
			    <tr>
				<td>Total Amount to Pay</td>
				<td><?= $amountUSD + $service_charge ?> approx</td>
			    </tr>
			    <tr>
				<td>Payment For</td>
				<td>

			    </tr>
			</tbody>
		    </table>
		</div>
	    </div>
	</div>
    </div>
</div>
<script src="<?= base_url() ?>assets/card/2co.js"></script>
<script src="<?= base_url() ?>assets/card/direct.min.js"></script>
<script>
		/*Called when token created successfully.*/
		var successCallback = function (data) {
		    var myForm = document.getElementById('myCCForm');

		    /*Set the token as the value for the token input
		     * Commented by Owden,enable progress bar
		     * */
		    //$('#span_loader').html('<div class="alert alert-info">Please wait...............</div>'+LOADER);
		    $('#span_loader').html('<div class="alert alert-info">Please wait...............</div>');
		    var token = data.response.token.token;
		    /*console.log(token);
		     IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.*/
		    $.ajax({
			url: $('#myCCForm').attr('action'),
			type: $('#myCCForm').attr('method'),
			data: 'token=' + token + '&invoice=<?= $invoice ?>',
			success: function (response) {
			    console.log(response);
			    $('#span_loader').html('');
			    $('#span_loader').html(response);
			    /*lets know the status of the payment. if its okay
			     then direct user to see a page with success, otherwise
			     show error message and allow user to make payment later*/
			},
			error: function (xhr, err) {
			    console.log(xhr);
			    $('#span_loader').html('<div class="alert alert-danger">' + xhr.errorMsg + '</div>');
			}
		    });
		    return false;
		    /*myForm.submit();*/
		};

		/*Called when token creation fails.*/
		var errorCallback = function (data) {
		    $('#span_loader').html('');
		    console.log(data);
		    if (data.errorCode === 200) {
			tokenRequest();
		    } else {
			$('#span_loader').html('<div class="alert alert-danger">' + data.errorMsg + '</div>');
		    }

		};

		var tokenRequest = function () {
		    /*Setup token request arguments*/
<?php
$mode = 'production';
$PUBLISHABLE_KEY = $mode == 'sandbox' ?
	'0FDA6E4F-0A21-41E4-B446-C89FC00D6735' :
	'BEC71F48-E7C1-11E4-901D-5ECC3A5D4FFE';
?>
		    var args = {
			sellerId: "901273428", /*this is seller ID change dep on sand or live*/
			publishableKey: "<?= $PUBLISHABLE_KEY ?>",
			ccNo: $("#ccNo").val(),
			cvv: $("#cvv").val(),
			expMonth: $("#expMonth").val(),
			expYear: $("#expYear").val()
		    };

		    /* Make the token request*/
		    TCO.loadPubKey('<?= $mode ?>', function () {
			TCO.requestToken(successCallback, errorCallback, args);
		    });
		};

		$(function () {
		    /*Pull in the public encryption key for our environment*/
		    TCO.loadPubKey('<?= $mode ?>');
		    $("#process-payment-btn").click(function (e) {
			/*Call our token request function
			 * Commented by Owden Godson
			 * */
			$('#span_loader').html('Loading...................');

			tokenRequest();
			$('#span_loader').html('');

			/*Prevent form from submitting*/
			return false;
		    });
		});
</script>