<div class="box">
    <!-- /.box-header -->
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("invoice/index") ?>"><?= $this->lang->line('menu_invoice') ?></a></li>
            <li class="active"><?= $this->lang->line('add_payment') ?></li>
        </ol>
    </div>


    <div class="box-body">
        <div class="row">

	    <div class="heading">
		<i class="circle-icon circle-teal mobile"></i>
		<h2 class="kfnkdjf">MOBILE Money Payment</h2>
	    </div>
	    <section class="widget widget-tabs col-md-8">
		<header>
		    <ul class="nav nav-tabs">
			<li class="active">
			    <a href="#stats" data-toggle="tab" aria-expanded="true">M-Pesa Payment</a>
			</li>
			<li class="">
			    <a href="#report" data-toggle="tab" aria-expanded="false">TigoPesa Payment</a>
			</li>
			<li class="">
			    <a href="#manual" data-toggle="tab" aria-expanded="false">Airtel Money Payment</a>
			</li>
		    </ul>
		</header>
		<div class="body tab-content">
		    <div id="stats" class="tab-pane clearfix active">
			<h2>M-pesa Payment instructions</h2>

			<p></p>
			<ol>
			    <li>Dial *150*00# to access your Vodacom Mpesa Menu</li>
			    <li>Select option 1, pay by M-pesa</li>
			    <li>Select option 4, Enter business number</li>
			    <li>Enter business number : <?= $invoice ?></li>
			    <li>Enter amount for your payment Tsh <?= number_format($amount) ?></li>
			</ol>
			 <p></p>
			<p>
			
			</p>
			
		    </div>
		    <div id="report" class="tab-pane">
			<h2>Tigo-pesa Payment instructions</h2>

			<p></p>
			<ol>
			    <li>Dial *150*01# to access your Tigo pesa Menu</li>
			    <li>Select option 4, for payment</li>
			    <li>Select option 2, to enter business number</li>
			    <li>Enter business number : <?= $invoice ?></li>
			    <li>Enter amount for your payment Tsh <?= number_format($amount) ?></li>
			</ol>
			<p></p>


		    </div>
		    <div id="manual" class="tab-pane">
			<h2>Airtel Money Payment</h2>
			<p></p>
			<p></p>
			<ol>
			    <li>Dial *150*60# to access your Airtel Money Menu</li>
			    <li>Select option 1, pay by Airtel Money</li>
			    <li>Select option 4, Enter business number</li>
			    <li>Enter business number : <?= $invoice ?></li>
			    <li>Enter amount for your payment Tsh <?= number_format($amount) ?></li>
			</ol>
			
			<p></p>
			<p>
			
			</p>
			
		    </div>

		</div>
	    </section>
	    <div class="col-md-4">
		<h4 class="heading">Payment Summary</h4>
		<table id="user" class="table table-bordered table-striped" style="clear: both">
		    <tbody>
			<tr>
			    <td class="column-left">Payment Amount:</td>
			    <td class="column-right">

				Tsh <?= number_format($amount); ?> 
			    </td>
			</tr>
			<tr>
			    <td>Service Charge</td>
			    <td>
				0
			    </td>
			</tr>
			<tr>
			    <td>Total Amount to Pay</td>
			    <td> Tsh <?= number_format($amount); ?> </td>
			</tr>
			<tr>
			    <td>Payment For</td>
			    <td>
				
			    </td>
			</tr>
		    </tbody>
		</table>
	    </div>
	</div>
    </div>
</div>