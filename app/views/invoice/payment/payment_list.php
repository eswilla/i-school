
<div class="row">
    <div class="col-md-12">
	<div class="page-header">
	    <h3 class="pmt" key="q11">Payment Requests</h3>
	    <small class="psm" key="P2">
		Select a business name and complete payment request</small>
	</div>

	<table class='table table-striped table-bordered table-hover table-full-width' id='sample_2'>
	    <thead>
		<tr>
		    <th class="trl" key="P4">Invoice</th>
		    <th class="trl" key="h9">Invoice Date</th>
		    <th class="trl" key="pfm">Payment For</th>
		    <th class='no-sort hidden-xs' key="P7">Payment Summary</th>
		    <th class='no-sort hidden-xs' key="pam">Total</th>
		    <th class="trl" key="b8">Action</th>
		    <th class="trl" key="P10">Time to Expire</th>
		</tr>
	    </thead>
	    <tbody>
		<?php
		if ($count > 0) {


		    foreach ($request->result() as $bn) {
			if ($bn->payment_ready == '0' && $bn->is_pay_confirmed == '0') {
			    $link = <<< EOF
       <button href="#" class="oneterm btn btn-primary btn-squared" key="py2" onclick="loadAjax('payment', 'apply', 'Business Name Payment', undefined, {'invoice':'{$bn->invoice}'});">Pay</button>
EOF;
			} elseif ($bn->payment_ready == '1' && $bn->is_pay_confirmed == '0') {
			    $link = <<< EOF
                            <button href="#" class="oneterm btn btn-info btn-squared" key="py3" onclick="loadAjax('payment', 'complete', 'Business Name Payment', undefined, {'invoice':'{$bn->invoice}','type':'mpesa'});">Confirm Payment</button>
EOF;
			} else {
			    $link = '';
			}
			?>
			<tr>
			    <td><?= $bn->invoice ?></td>
			    <td><?=date('d  F Y ', strtotime($bn->book_date))?></td>
			    <th><?= $bn->name ?></th>
			    <td>
				<?php
				$data = "{'data': [" . $bn->summary . "]}";
				$string = str_ireplace("'", '"', $data);
				$json = json_decode($string, TRUE);
				$payment_for = '';
				foreach ($json['data'] as $value) {
				    $payment_for.=$value['description'] . ': Tsh' . $value['amount'] . '/=<br/>';
				}
				?>
				<?= $payment_for ?>
			    </td>
			     <td>Tsh<?= $bn->amount ?>/=</td>
			    <td><?= $link ?></td>
			    <td><?= date('i', time() - strtotime($bn->book_date)) ?> min</td>
			</tr>
			<?php
		    }
		    echo " </tbody>
                </table>";
		}
		?>
	    <hr>

	    <div id = "bn_name_app_review">

	    </div>

	    <!--            </div>
		    </div>-->
	    <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?= site_url(); ?>assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script>
    jQuery(document).ready(function () {
	TableData.init();
    });
</script>