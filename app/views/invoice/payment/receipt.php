<html>

    <head>

	<title>receipt</title>

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<style type="text/css">

	    TABLE {

		padding:5px;

		font-family:Arial, Helvetica, sans-serif;

		font-size:11px;

		height:auto;

		padding-bottom:20px;

	    }

	    TABLE LABEL {

		display:block;

		width:250px;

		float:left

	    }

	    TABLE LABEL SPAN {

		font-style:italic;

	    }

	    TABLE SPAN {

		display:block;

		vertical-align:text-bottom

	    }



	</style>

    </head>
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table id="Table_01" width="600" height="800" align="center" border="0" cellpadding="0" cellspacing="0">

	    <tr>
               
		<td width="600"  height="100" valign="top" align="right">

                    	<table width="600" height="70" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="200">
                                    <img src="<?= base_url()?>assets/images/OBRS.png" alt="<?=TITLE?>" width="200" height="70" />
                                </td>
                                
                                <td>
		    <div style="font-family:tahoma; font-size:10px; display:block; padding-left:60px">

			<b>WAKALA WA USAJILI WA BIASHARA NA LESENI.</b><br>

			<b>S.L.P</b> 9393, Dar-es-Salaam.<br>

			<b>Simu:</b> +255 22 2180141, 2180113, 2181113.&nbsp;&nbsp;<b>Faksi:</b> +255 22 2180371<br>

			<b>Barua pepe:</b> usajili@cats-net.com, brela@cats-net.com<br>

			<b>Tovuti:</b> http://www.<?=TITLE?>.go.tz

		    </div></td>

               

	    </tr>
                        </table>
                </td>
            </tr>
	    <tr>

		<td width="600"  colspan="2" valign="top">



		    <table width="580" ><tr>

			    <td colspan="3">

				<label>

				    NIMEPOKEA KWA<BR>

				    <span>Received from</span>

				</label>

				<span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
				    <?=$name?></span>

				<span>..................................................................................................................................................................................................



				</span>

			    </td></tr>

			<tr><td colspan="3">



				<label>

				    KIASI CHA SHILINGI(KWA MANENO)<BR>

				    <span>Sum of shillings(words)</span>

				</label>

				<span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
				    <?=$amount?></span>

				<span>.................................................................................................................................................................................................

				</span>



			    </td></tr>

			<tr><td colspan="3">



				<label>

				    KWA MALIPO YA <BR>

				    <span>In respect of</span>

				</label>

				<span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
				    <?=$payment_for?></span>

				<span>.................................................................................................................................................................................................

				</span>



			    </td></tr>

			<tr><td colspan="3">



				<label>

				    KWA FEDHA TASLIMU/HUNDI NAMBA <BR>

				    <span>By cash/cheque No.</span>

				</label>

				<span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
				    <?=$checkue_number?></span>

				<span>.................................................................................................................................................................................................

				</span>
			    </td></tr>

			<tr><td colspan="3">



				<label>

				    KITUO <BR>

				    <span>Station</span>

				</label>

				<span style="padding-left:200px; font-weight:bold">HEAD QUARTERS</span>

				<span>.................................................................................................................................................................................................

				</span>



			    </td></tr>

			<tr><td width="200">

				SAHIHI YA MPOKEAJI<br>

				<font style="font-style:italic">Receiving officer's sign.</font>

				<span style="text-transform:uppercase; font-weight:bold; display:block; padding-top:10px">.........................................</span>

			    </td>

			    <td width="200">

				CHEO<br>

				<font style="font-style:italic">Title</font>

				<span style="text-decoration:underline; text-transform:uppercase; font-weight:bold; padding-top:10px; display:block">ACCOUNTANT</span>

			    </td>

			    <td width="200">

				TAREHE<br>

				<font style="font-style:italic">Date</font>

				<span style="text-decoration:underline; text-transform:uppercase; font-weight:bold; display:block; padding-top:10px"><?=  date('d-m-Y')?></span>

			    </td></tr>



		    </table>



		</td>

	    </tr>

	    <tr>

                <td colspan="2">
</td>

	    </tr>

	</table>
    </body>

</html>