
<div class="box">
    <!-- /.box-header -->
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("invoice/index") ?>"><?= $this->lang->line('menu_invoice') ?></a></li>
            <li class="active"><?= $this->lang->line('add_payment') ?></li>
        </ol>
    </div>


    <div class="box-body">
        <div class="row">
	    <div class="heading">
		<i class="nmb circle-icon circle-teal"></i>
		<h2>NMB BANK</h2>
	    </div>
	    <section class="widget widget-tabs col-md-8">
		<header>
		    <ul class="nav nav-tabs">
			<li class="active">
			    <a href="#stats" data-toggle="tab" aria-expanded="true">NMB Mobile</a>
			</li>
			<li class="">
			    <a href="#report" data-toggle="tab" aria-expanded="false">BRANCH Payment</a>
			</li>
			<li class="dropdown">
			    <a href="#manual" data-toggle="tab" aria-expanded="false">NMB Wakala</a>
			</li>
		    </ul>
		</header>
		<div class="body tab-content">
		    <div id="stats" class="tab-pane clearfix active">
			<h3 class="tt" key="nmb1">How to do Payment</h3>

			<p></p>
			<ol>
			    <li class="nmb3" key="step1">Dial *150*66# in your phone to open NMB mobile service</li>
			    <li class="nmb3" key="step2">Enter your secret PIN then OK to accept</li>
			    <li class="nmb3" key="step3">Select (5) for Bills Payment</li>
			    <li class="nmb3" key="step4">Select (6) option for <?= $TITLE ?></li>
			    <li><span class="nmb3" key="step5">Enter invoice number </span>: <span class="notranslate"><?= $invoice ?></span></li>
			    <li><span  class="nmb3" key="step6">Enter amount for your payment </span> <span class="notranslate">Tsh<?= number_format($amount) ?></span></li>
			</ol>
			
			<p></p>
			
			
		    </div>
		    <div id="report" class="tab-pane">
			<h3>BRANCH Payment instructions</h3>

			<p></p>
			<ol>
			    <li>Visit any nearby NMB BANK branch</li>
			    <li>Make deposit by specify invoice number : <?= $invoice ?> with payment amount of Tsh <?= number_format($amount) ?></li>
			</ol>
			  <p></p>


			

			
		    </div>
		    <div id="manual" class="tab-pane">
			<h3>NMB Wakala Payment instructions</h3>
			<p></p>
			<ol>
			    <li>Visit any nearby NMB Wakala agent</li>
			    <li>Make deposit of Tsh <?= number_format($amount) ?> and specify invoice number : <?= $invoice ?></li>
			</ol>
			
		    </div>

		</div>
	    </section>
	    <div class="col-md-4">
		<h4 class="heading" key="psm">Payment Summary</h4>
		<table id="user" class="table table-bordered table-striped" style="clear: both">
		    <tbody>
			<tr>
			    <td class="column-left"><span class="s5"  key="pam">Payment Amount</span>:</td>
			    <td class="column-right">

				Tsh <?= number_format($amount); ?> 
			    </td>
			</tr>
			<tr>
			    <td><span  class="s5"  key="psc">Service Charge</span></td>
			    <td>
				0
			    </td>
			</tr>
			<tr>
			    <td><span class="s5"  key="ptp">Total Amount to Pay</span></td>
			    <td> Tsh <?= number_format($amount) ?> </td>
			</tr>
			<tr>
			    <td><span class="s5"  key="pfm">Payment For</span></td>
			    <td>
				
			    </td>
			</tr>
		    </tbody>
		</table>
	    </div>
	</div>
    </div>
</div>