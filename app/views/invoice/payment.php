
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("invoice/index") ?>"><?= $this->lang->line('menu_invoice') ?></a></li>
            <li class="active"><?= $this->lang->line('add_payment') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">

		<?php
		$usertype = $this->session->userdata("usertype");
		if ($usertype == "Admin" || $usertype == "Accountant") {

		    /**
		     * ----------------------------------------------------------------
		     * In case a student bring cash, accountant or admin can manually
		     * add that amount to a student account. This is not much a good
		     * process just beacause it is associated with much security issues
		     * ----------------------------------------------------------------- 
		     * 
		     */
		    ?>
    		<form class="form-horizontal" role="form" method="post">

			<?php
			if (form_error('amount'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
    		    <label for="amount" class="col-sm-2 control-label">
			    <?= $this->lang->line("invoice_amount") ?>
    		    </label>
    		    <div class="col-sm-6">
    			<input type="text" class="form-control" id="amount" name="amount" value="<?= set_value('amount', $invoice->amount - $invoice->paidamount) ?>" >
    		    </div>
    		    <span class="col-sm-4 control-label">
			    <?php echo form_error('amount'); ?>
    		    </span>
    	    </div>

		<?php
		if (form_error('payment_method'))
		    echo "<div class='form-group has-error' >";
		else
		    echo "<div class='form-group' >";
		?>
    	    <label for="payment_method" class="col-sm-2 control-label">
		    <?= $this->lang->line("invoice_paymentmethod") ?>
    	    </label>
    	    <div class="col-sm-6">
		    <?php
		    $array = $array = array('0' => $this->lang->line("invoice_select_paymentmethod"));
		    $array['Cash'] = $this->lang->line('invoice_cash');
		    $array['Cheque'] = $this->lang->line('invoice_cheque');
		    $array['Paypal'] = $this->lang->line('invoice_paypal');
		    echo form_dropdown("payment_method", $array, set_value("payment_method"), "id='payment_method' class='form-control'");
		    ?>
    	    </div>
    	    <span class="col-sm-4 control-label">
		    <?php echo form_error('payment_method'); ?>
    	    </span>
    	</div>

    	<div class="form-group">
    	    <div class="col-sm-offset-2 col-sm-8">
    		<input type="submit" class="btn btn-success" value="<?= $this->lang->line("add_payment") ?>" >
    	    </div>
    	</div>

    	</form>
	<?php
	} elseif ($usertype == "Student" || $usertype == "Parent") {
	    /**
	     * -------------------------------------------------------------
	     * This is the recommended procedure of our system. To allow
	     * electronic payment to be done and amount to be deposited
	     * to school bank account
	     * -------------------------------------------------------------
	     */
	    ?>
    	<!-- start: PAGE HEADER -->
	    </div>
</div>
    	<div class="row">
    	    <div class="col-sm-12">
    		<!-- start: STYLE SELECTOR BOX -->

    		<div class="page-header">
    		    <h1 class="v2"><span class="trl" key="l7">Payment </span>
    			<small class="trl" key="z2">Choose Payment method you Prefer</small></h1>
    		</div>
    		<!-- end: PAGE TITLE & BREADCRUMB -->
    	    </div>
    	</div>
    	<!-- end: PAGE HEADER -->
    	<!-- start: PAGE CONTENT -->
    	<div class="col-lg-12">
	    <div class="col-lg-12" >
    		<div class="col-sm-4"> <!--to be changed into col-sm-4 when we add other payment systems-->
    		    <div class="core-box">
    			<div class="heading">
    			    <?=img(base_url('uploads/images/crdb.png'))?>
    			    <h2>CRDB BANK </h2>
    			</div>
    			<div>
    			    <p class="op" key="op1">You can use either CRDB SIM BANKING, DIRECT BANK DEPOSIT or FAHARI HUDUMA Payment Method.</p>
    			</div>
    			<a class="notranslate view-more btn btn-success btn-xs" href="<?=base_url("invoice/method/crdb?iid=".$invoice->invoiceID)?>" >
    			    <span class="trl" key="fw1" >Continue</span> <i class="clip-arrow-right-2"></i>
    			</a>
    		    </div>
    		</div>
    		<div class="col-sm-4">
    		    <div class="core-box">
    			<div class="heading">
    			   <?=img(base_url('uploads/images/nmb.png'))?>
    			    <h2>NMB BANK </h2>
    			</div>
    			<div>
    			    <p class="op" key="op2">You can use either NMB MOBILE BANKING, DIRECT BANK BRANCH DEPOSIT or NMB WAKALA Payment Method.</p>
    			</div>
    			<a class="notranslate view-more btn btn-success btn-xs" href="<?=base_url("invoice/method/nmb?iid=".$invoice->invoiceID)?>">
    			    <span class="trl" key="fw1"> Continue</span> <i class="clip-arrow-right-2"></i>
    			</a>
    		    </div>
    		</div>

    		<div class="col-sm-4">
    		    <div class="core-box">
    			<div class="heading">
    			    <?=img(base_url('uploads/images/mobile.png'))?>
    			    <h2>MOBILE Money</h2>
    			</div>
    			<div class="">
    			    If you are using mobile payment solution, you can make payment via M-PESA or Tigo-Pesa Payment system.
    			</div>
    			<a class="view-more btn btn-success btn-xs" href="<?=base_url("invoice/method/mobile?iid=".$invoice->invoiceID)?>">
    			    Continue <i class="clip-arrow-right-2"></i>
    			</a>
    		    </div>
    		</div>
    	    </div>
    	</div>

    	<div class="row">

    	    <div class="col-sm-4"></div>
    	    <div class="col-sm-4">
		<br/>
    		<div class="core-box">
    		    <div class="heading">
    			<?=img(base_url('uploads/images/online.png'))?>
    			<h2>BANK CARDS</h2>
    		    </div>
    		    <div class="">
    			•Visa
    			•	MasterCard
    			•	Discover
    			•	American Express
    			•	Diners
    			•	JCB
    			•	PIN debit cards with the Visa or MasterCard logo
    			•	Debit cards with the Visa or MasterCard logo
    			•	PayPal
    		    </div>
    		    <a class="view-more btn btn-success btn-xs" href="<?=base_url("invoice/method/card?iid=".$invoice->invoiceID)?>">
    			Continue <i class="clip-arrow-right-2"></i>
    		    </a>
    		</div>
    	    </div>

    	</div>
	<?php } ?>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#classesID').change(function (event) {
	var classesID = $(this).val();
	if (classesID === '0') {
	    $('#studentID').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('invoice/call_all_student') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    $('#studentID').html(data);
		}
	    });
	}
    });


    $('#feetype').keyup(function () {
	var feetype = $(this).val();
	$.ajax({
	    type: 'POST',
	    url: "<?= base_url('invoice/feetypecall') ?>",
	    data: "feetype=" + feetype,
	    dataType: "html",
	    success: function (data) {
		if (data != "") {
		    var width = $("#feetype").width();
		    $(".book").css('width', width + 25 + "px").show();
		    $(".result").html(data);

		    $('.result li').click(function () {
			var result_value = $(this).text();
			$('#feetype').val(result_value);
			$('.result').html(' ');
			$('.book').hide();
		    });
		} else {
		    $(".book").hide();
		}

	    }
	});
    });

    $('#date').datepicker();
</script>