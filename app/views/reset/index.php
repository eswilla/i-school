
<div class="form-box" id="login-box">
    <div>Reset Password</div>
    <form role="form" method="post" class="form-horizontal">
        <div class="body white-bg">

	    <?php
	    if ($form_validation == "No") {
		
	    } else {
		if (count($form_validation)) {
		    echo "<div class=\"alert alert-danger alert-dismissable\">
                            <i class=\"fa fa-ban\"></i>
                            <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                            $form_validation
                        </div>";
		}
	    }

	    if ($this->session->flashdata('reset_send')) {
		$message = $this->session->flashdata('reset_send');
		echo "<div class=\"alert alert-success alert-dismissable\">
                        <i class=\"fa fa-ban\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $message
                    </div>";
	    } else {
		if ($this->session->flashdata('reset_error')) {
		    $message = $this->session->flashdata('reset_error');
		    echo "<div class=\"alert alert-danger alert-dismissable\">
	                        <i class=\"fa fa-ban\"></i>
	                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
	                        $message
	                    </div>";
		}
	    }
	    ?>
            <div class="form-group">
		<label for="inputName" class="col-md-2 control-label">
</label>
		<div class="col-md-10">
		    <div class="input-icon right">
			<i class="fa fa-user"></i>
			<input class="form-control" placeholder="Email" name="email" type="text" id="inputName">
		    </div>
		</div>
            </div>   
	    <div class="form-group mbn">
			<label for="inputName" class="col-md-2 control-label">
</label>
		<div class="col-lg-10">
		    <div class="form-group mbn">
			
			<div class="col-lg-6 center center-justified">
			    <input type="submit" class="btn btn-default sign-btn" value="Send" />
			</div>
		    </div>
		</div>
	    </div>
        </div>
    </form>
</div>