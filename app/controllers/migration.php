<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    public function __construct() {
	parent::__construct();
    }

    public function index() {
	$this->load->library('migration');

	if (!$this->migration->current()) {
	    show_error($this->migration->error_string());
	} else {
	    echo "success";
	}
    }

}

/* End of file migration.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/migration.php */