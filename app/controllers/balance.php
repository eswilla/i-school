<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Balance extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$this->load->model("classes_m");
	$this->load->model("student_m");
	$this->load->model("section_m");
	$language = $this->session->userdata('lang');
	$this->lang->load('balance', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['set'] = $id;
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['students'] = $this->student_m->get_order_by_student(array('classesID' => $id));

		if ($this->data['students']) {
		    $sections = $this->section_m->get_order_by_section(array("classesID" => $id));
		    $this->data['sections'] = $sections;
		    foreach ($sections as $key => $section) {
			$this->data['allsection'][$section->section] = $this->student_m->get_order_by_student(array('classesID' => $id, "sectionID" => $section->sectionID));
		    }
		} else {
		    $this->data['students'] = NULL;
		}

		$this->data["subview"] = "balance/index";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data['classes'] = $this->classes_m->get_classes();
		$this->data["subview"] = "balance/search";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function balance_list() {
	$classID = $this->input->post('id');
	if ((int) $classID) {
	    $string = base_url("balance/index/$classID");
	    echo $string;
	} else {
	    redirect(base_url("balance/index"));
	}
    }

}

/* End of file balance.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/balance.php */