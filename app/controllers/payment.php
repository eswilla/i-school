<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @author Ephraim Swilla <swillae1@gmail.com, ephraim@inetstz.com>
 * 
 * NMB
 * Private IP=192.168.60.232/233
 * Public IP=41.221.61.121
 */
class Payment extends Admin_Controller {

    /**
     *
     * @var Integer 
     */
    public $transaction_id;

    /**
     *
     * @var Integer 
     */
    public $payment_id;

    /**
     *
     * @var Mixed 
     * @uses show the invoice number to accept payment,
     *       the invoice is used in communicaiton with bank to 
     *       identify who pays and is unique      
     */
    private $invoice;

    /**
     *
     * @var Mixed 
     */
    private $HEADER = array(
	'application/x-www-form-urlencoded'
    );

    /**
     *
     * @var Mixed parameters received in api 
     */
    private $api_param;

    /**
     *
     * @var Mixed  
     */
    private $api_info;

    /**
     * @var Client ID
     */
    public $client_id;

    /**
     * @access public : Load payment modal and initialize transaction ID
     * 
     */
    public function __construct() {
	parent::__construct();
	$this->load->model('Payment_modal');
	//$this->output->enable_profiler(TRUE);
	// ha haaaaa, na detect issues and fix if someone want to access other method without login

	$this->client_id = $this->session->userdata('client_id');
	$pay_token = $this->session->userdata('pay_token');
    }

    /**
     * @uses Initial loading of appliation 
     */
    public function index() {
	//$id = $this->input->get_post('bnm_app_id');
	//$app_id = preg_replace('/\D/', '', $id);
	$invoice = $this->input->get_post('invoice');
	//$app_data = $this->Payment_modal->fetch_payment_data($app_id);
	$app_data = $this->Payment_modal->fetch_payment_data_by_invoice($invoice);
	if (!empty($app_data)) {
	    $data['amount'] = $app_data->amount; //to be changed in a database
	    $data['invoice'] = $app_data->invoice;
	    $data['booking_id'] = $app_data->booking_id;
	    // $data['bnm_app_id'] = $app_data->bnm_app_id; //removed
	    $data['summary'] = $app_data->summary;

	    $page = $this->input->get_post('pg');
	    if (preg_match('/nmb/', $page)) {
		$this->load->view('nmb', $data);
	    } elseif (preg_match('/crdb/', $page)) {
		$this->load->view('crdb', $data);
	    } else if (preg_match('/mobile/', $page)) {
		$this->load->view('payment', $data);
	    } else {
		$this->load->view('request', $data);
	    }
	} else {
	    echo '<div class="alert alert-warning"><button data-dismiss="alert" class="close">×</button>
		<i class="fa fa-exclamation-triangle"></i><strong>Warning ! </strong> &nbsp Sorry, you have no payment request</div>';
	}
    }

    public function payment_list() {

	$data['request'] = $this->db->query("SELECT p.invoice,p.reg_date,p.summary,p.amount,p.name,p.payment_ready,p.is_pay_confirmed, b.book_date,b.currency FROM  payment_requests p JOIN booking b ON p.booking_id=b.booking_id WHERE p.client_id ='" . $this->client_id . "'AND  p.is_pay_confirmed='0' AND p.invoice !=''");
	$data['count'] = $data['request']->num_rows();
	$this->load_view('payment_list', $data);
    }

    public function complete() {
	$data['invoice'] = $this->input->get_post('invoice');
	$name = $this->input->get_post('name');
	$data['bname'] = empty($name) ? "BRELA" : $name;
	// select from a table
	$this->load->view('confirm', $data);
    }

    public function get_payment_request() {
	$request = $this->Payment_modal->get_payment_status();
    }

    public function history() {
	if (!empty($this->client_id)) {
	    $data['history'] = $this->Payment_modal->get_history();
	    $this->load->view('history', $data);
	}
    }

    public function confirm_payment() {
	$info = $this->Payment_modal->validate($this->input->get_post('code'), $this->input->get_post('invoice'));
	if ($info->control == 0) {
	    // no record found
	    echo json_encode(array(
		'status' => 0,
		'message' => '<div class="alert alert-danger">Invalid receipt number</div>'
	    ));
	} else if ($info->control == 1) {
	    echo json_encode(array(
		'status' => 0,
		'message' => '<div class="alert alert-danger">Invalid receipt number for this payment</div>'
	    ));
	} else if ($info->control == 2) {
	    //later we will put send_pdf_receipt here
	    $data = $this->Payment_modal->get_booking($this->input->get_post('invoice'));
	    $this->process_transaction($data->payment_id, $data->booking_id);
	}
    }

    public function process_transaction($payment_id, $booking_id) {
	$this->db->update('payment', array('status' => '1'), array('payment_id' => $payment_id));
	$this->db->update('bnm_app', array('is_pay_confirmed' => '1'), array('booking_id' => $booking_id));
	//after successful update, send a receipt to user email
	$send = $this->send_pdf_receipt($payment_id);
	//$bn=  $this->Payment_modal->get_business_by_booking_id($payment_id);

	echo json_encode(array(
	    'status' => 1,
	    'message' => '<div class="alert alert-success">Payment successfully  accepted, receipt file has been sent to your email address </div>'
	));

//	  $link = <<< EOF
//       <button class="btn btn-warning btn-squared" onclick="loadAjax('bn_application', 'register', 'Register Business', undefined, {'name': '{$bn->name}','doc':'{$bn->doc}','bnm_app_id':{$bn->bnm_app_id}});">Register Now</button>
//EOF;
//	  echo $link;
	return true;
    }

    public function send_pdf_receipt($payment_id) {

	$html = __DIR__ . '../../views/receipt.php';
	$app_data = $this->Payment_modal->get_receipt_info($payment_id);


	$name = $app_data->name;
	$currency = ' SHILLINGS ONLY';
	$amount = $this->convert_number($app_data->amount) . $currency;

	$data = "{'data': [" . $app_data->summary . "]}";
	$string = str_ireplace("'", '"', $data);
	$json = json_decode($string, TRUE);
	$payment_for = '';
	foreach ($json['data'] as $value) {
	    $payment_for.=$value['description'] . ' [' . $value['amount'] . '], <br/> ';
	}
	$payment_for.=' Total [' . $app_data->amount . ']';
	$checkue_number = $app_data->code;
	$receipt_number = $app_data->receipt_id;
	ob_start();
	include_once $html;
	$data = ob_get_clean();


	$system_root_file = dirname(__FILE__) . '/../../../data/payment_receipt/' . time() . '.pdf';
	$this->generate_file($system_root_file, $data);
	chmod($system_root_file, '0777');

	if ($app_data->client_id === NULL) {
	    $user = $this->Payment_modal->get_email_from_search($app_data->booking_id);
	} else {
	    $user = $this->Payment_modal->get_client_info($app_data->client_id);
	}
	$name = str_replace(dirname(__FILE__) . '/../../../', base_url() . 'mfumo/', $system_root_file);
	$this->update_receipt_status($name, $app_data->receipt_id);
	echo $name;
	//$this->send_email($app_data->client_id,$user->email, 'Business Name Payment Receipt', $message, $name, $system_root_file);
	//unlink($system_root_file);
    }

    private function update_receipt_status($name,$receipt_id){
	$this->db->update('receipt',array('file_location'=>$name),array('receipt_id'=>$receipt_id));
    }
    private function generate_file($system_root_file, $data) {
	//$data=  file_get_contents($html);
	$this->load->helper('dompdf/pdf');

	$dompdf = new DOMPDF();
	$dompdf->load_html($data);
	$dompdf->render();
	$output = $dompdf->output();
	file_put_contents($system_root_file, $output);
    }

    public function apply() {
	//bnm_app_id
	/*
	 * this method should now receive an invoice only, then, from that, we 
	 * can derive any other parameter we need since its a unique values
	 */
	$data['invoice'] = $this->input->get_post('invoice');
	if (empty($data['invoice'])) {
	    die('You have no payment request');
	}
	$this->load->view('request', $data);
    }

    function reserve_company() {
	$cmp_app_id = $this->input->get_post('cmp_app_id');
	$this->db->query("SELECT create_booking_cmp(" . $cmp_app_id . ")");
	$info = $this->db->query("SELECT invoice FROM cmp_app_status WHERE cmp_app_id='" . $cmp_app_id . "' ")->row();
	$data['invoice'] = $info->invoice;
	$this->load->view('request', $data);
    }

    public function dashboard() {
	$this->load->view('dashboard');
    }

    /**
     * 
     * @return int : Request position number
     */
    private function init_api_request() {
	$api_key = $this->input->get_post("api_key");
	$api_secret = $this->input->get_post("api_secret");
	// first save all information received via API request
	//$this->save_api_request($api_key, $api_secret);
	//check keys if they exist and get user information
	$this->api_info = $this->Payment_modal->get_api_user($api_key, $api_secret);

	if (empty($this->api_info)) {
	    $data['request'] = 1;
	    $data['status'] = 203;
	    $data['param'] = array(
		'invoice' => $this->api_param['invoice']
	    );
	    die(json_encode($data));
	}
	$this->api_param = $_REQUEST['param'];
	return $_REQUEST['request'];
    }

    private function save_api_request($api_key = '', $api_secret = '') {
	$ip = $_SERVER['REMOTE_ADDR'];

	$host = gethostbyaddr($ip);

	$this->db->insert('pay', array(
	    'key' => $api_key,
	    'secret' => $api_secret,
	    'content' => 'transaction ID=' . $this->input->get_post('transaction_id') . ' method=' . $this->input->get_post('method') . ' branch name=' . $this->input->get_post('branch_name'),
	    'header' => 'REMOTE_ADDR=' . $this->input->get_post('REMOTE_ADDR') . ' REMOTE_PORT=' . $this->input->get_post('REMOTE_PORT'),
	    'remote_ip' => $this->get_remote_ip(),
	    'remote_hostname' => $host
	));
	//Force security measures
    }

    /**
     * @description: The method forces and ensure that uniformity and security is followed
     *                 No request will be accepted if it does not follow these methods
     */
    private function force_security_measures() {
	//force all requests to use POST method
	if (strtoupper($_SERVER['REQUEST_METHOD']) != 'POST') {
	    die("Follow API standards when you make requests to this server. This request method is not accepted");
	}
    }

    private function get_remote_ip() {
	if (getenv('HTTP_CLIENT_IP')) {
	    $ip = getenv('HTTP_CLIENT_IP');
	} elseif (getenv('HTTP_X_FORWARDED_FOR')) {
	    $ip = getenv('HTTP_X_FORWARDED_FOR');
	} elseif (getenv('HTTP_X_FORWARDED')) {
	    $ip = getenv('HTTP_X_FORWARDED');
	} elseif (getenv('HTTP_FORWARDED_FOR')) {
	    $ip = getenv('HTTP_FORWARDED_FOR');
	} elseif (getenv('HTTP_FORWARDED')) {
	    $ip = getenv('HTTP_FORWARDED');
	} else {
	    $ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
    }

    function db_status_connect() {
	if ($this->api_info->sand_box == 1) {
	    $request_no = $_REQUEST['request'];
	    $fields = array(
		'request' => $request_no,
		'api_secret' => $this->api_info->api_secret,
		'api_key' => $this->api_info->api_key,
		'param' => $this->api_param
	    );
	    echo $this->curl_test_server($fields);
	    exit;
	}
    }

    /**
     * 
     * @param type $fields
     */
    private function curl_test_server($fields) {
	// Open connection
	$ch = curl_init();
	// Set the url, number of POST vars, POST data

	curl_setopt($ch, CURLOPT_URL, 'http://192.168.2.13/payment/api');
	curl_setopt($ch, CURLOPT_HTTPHEADER, $this->HEADER);

	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
    }

    /**
     * @return JSON OBJECT
     */
    private function payment_first_request() {

	//$this->db_status_connect();

	$invoice = $this->Payment_modal->get_booking(strtoupper($this->api_param['invoice']));
	if (!empty($invoice)) {
	    //this invoice exist, so lets return it to a client with amount to process
	    $data['request'] = 1;

	    if ($invoice->payment_status == 1) {
		// this invoice has already being paid
		$data['status'] = 204;
		$data['param'] = array(
		    'invoice' => $this->api_param['invoice'],
		);
	    } else {
		// this invoice is not yet being paid
		$data['status'] = 201;
		$data['param'] = array(
		    'invoice' => $this->api_param['invoice'],
		    'amount' => $invoice->amount,
		    'currency' => $invoice->currency,
		    'account' => $invoice->code,
		    'name' => $invoice->name
		);
	    }

	    echo json_encode($data);
	} else {
	    $data['request'] = 1;
	    $data['status'] = 202;
	    $data['param'] = array(
		'invoice' => $this->api_param['invoice']
	    );
	    echo json_encode($data);
	}
    }

    function check_empty_param() {
	$mandatory_fields = array(
	    'transaction_id', 'amount', 'account_number', 'method', 'status', 'currency', 'invoice'
	);
	foreach ($mandatory_fields as $key) {
	    if (!isset($this->api_param[$key])) {
		die($key . ' is empty. Mandatory fields includes ' . json_encode($mandatory_fields));
	    }
	}
    }

    /**
     * @return JSON OBJECT 
     */
    private function payment_second_request() {
	$this->check_empty_param();
	$invoice = $this->Payment_modal->get_booking($this->api_param['invoice']);
	if (!empty($invoice)) {
	    // This is when a bank return payment status to us
	    //save it in the database
	    $phone_number = isset($this->api_param['phonenumber']) ? $this->api_param['phonenumber'] : '';
	    $mobile_transaction_id = isset($this->api_param['mobile_transaction_id']) ?
		    $this->api_param['mobile_transaction_id'] : '';

	    $query = $this->db->query("select payment_id, control from receive_payment('{$this->api_param['invoice']}', "
			    . "'{$phone_number}', "
			    . "'{$this->api_param['transaction_id']}', "
			    . "'{$mobile_transaction_id}', "
			    . "{$this->api_param['amount']}, "
			    . "'{$this->api_info->service_provider}',"
			    . "'{$this->api_param['account_number']}',"
			    . "'{$this->api_param['method']}',"
			    . "'{$this->api_param['status']}',"
			    . "'{$this->api_param['currency']}')")->row();
	    //get receipt information

	    if ($query->control == 1) {
		//successful,
		$receipt = $this->Payment_modal->get_receipt_info($query->payment_id);


		//send SMS to users who have the invoice, and not the phone number used to make payment
		$message = "RECEIPT : " . $receipt->code . " confirmed. " . $this->api_param['currency'] . " " . $this->api_param['amount'] . "/= received by BRELA for " . $receipt->name . " on " . date('d/m/Y') . " at " . date('h:i:s A') . " INVOICE: " . $this->api_param['invoice'];
		// $user->email


		$user = $this->Payment_modal->get_client_info($receipt->client_id);

		$this->send_email($receipt->client_id, $user->email, 'ORS Payment Receipt', $message, $user->firstname . ' ' . $user->lastname);
		$this->send_sms($receipt->client_id, $user->phone_number, $message);
		//return feedback to a bank so he can send SMS too
		$data['request'] = 2;
		$data['status'] = 201;
		$data['param'] = array(
		    'invoice' => $this->api_param['invoice'],
		    'receipt' => $receipt->code
		);
		echo json_encode($data);
	    } else if ($query->control == 2) {
		//payment is duplicated, already received
		$user = $this->Payment_modal->get_receipt_from_booking($this->api_param['invoice']);

		$data['request'] = 2;
		$data['status'] = 204;
		$data['param'] = array(
		    'invoice' => $this->api_param['invoice'],
		    'receipt' => $user->code
		);
		echo json_encode($data);
	    } else if ($query->control == 3) {
		// invoice comes with amount which is not valid

		$data['request'] = 2;
		$data['status'] = 203;
		$data['param'] = array(
		    'invoice' => $this->api_param['invoice'],
		);
		echo json_encode($data);
	    }
	} else {
	    //we don't have this invoice
	    $data['request'] = 2;
	    $data['status'] = 202;
	    $data['param'] = array(
		'invoice' => $this->api_param['invoice'],
	    );
	    echo json_encode($data);
	}
    }

    /**
     * @return JSON OBJECT
     */
    public function api() {
	/*	 * -------------payment flow will be as follows-----------------

	 * 1. validate api_key and api_secret by checking in the database
	  2. if the request=1, take the invoice form the database and send it to client with
	  request id, status,param (invoice, amount,currency,account)
	  3. if request is 2, then we expect to receive payment information from a bank
	  and the status if the payment is complete or not, if complete, we return
	  to client request, status, param (invoice, receipt)
	 */
	$request = $this->init_api_request();
	//$this->db_status_connect();
	switch ($request) {
	    case 1:
		//this is the first request from a bank
		// fetch user request and send parameters to us
		$this->payment_first_request();
		break;
	    case 2:
		//this is the second request from a bank with the same invoice no
		$this->payment_second_request();
		break;
	    default:
		$data['request'] = 1;
		$data['status'] = 203;
		$data['param'] = array(
		    'invoice' => $this->api_param['invoice']
		);
		echo json_encode($data);
		break;
	}
    }

    /**
     * 
     * @param type $input string
     * @return type
     */
    function encrypt($input) {
	return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, hash('sha256', ENCRYPTION_KEY, TRUE), $input, MCRYPT_MODE_ECB, mcrypt_create_iv(32)));
    }

    /**
     * @used Generate payment receipt to a user
     * @depends payment_insertion
     * @var session_id=session from someone who log in
     * @var payment_id
     */
    function code($nc = 6, $a = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789') {
	$l = strlen($a) - 1;
	$r = '';
	while ($nc-- > 0)
	    $r.=$a{mt_rand(0, $l)};

	return $r;
    }

    /*
     * *  Function:   convert_number
     * *  Arguments:  int
     * *  Returns:    string
     * *  Description:
     * *      Converts a given integer (in range [0..1T-1], inclusive) into
     * *      alphabetical format ("one", "two", etc.).
     */

    public function convert_number($number) {
	if (($number < 0) || ($number > 999999999)) {
	    return "$number";
	}

	$Gn = floor($number / 1000000);  /* Millions (giga) */
	$number -= $Gn * 1000000;
	$kn = floor($number / 1000);     /* Thousands (kilo) */
	$number -= $kn * 1000;
	$Hn = floor($number / 100);      /* Hundreds (hecto) */
	$number -= $Hn * 100;
	$Dn = floor($number / 10);       /* Tens (deca) */
	$n = $number % 10; /* Ones */

	$res = "";

	if ($Gn) {
	    $res .= $this->convert_number($Gn) . " Million";
	}

	if ($kn) {
	    $res .= (empty($res) ? "" : " ") .
		    $this->convert_number($kn) . " Thousand";
	}

	if ($Hn) {
	    $res .= (empty($res) ? "" : " ") .
		    $this->convert_number($Hn) . " Hundred";
	}

	$ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
	    "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
	    "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
	    "Nineteen");
	$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
	    "Seventy", "Eigthy", "Ninety");

	if ($Dn || $n) {
	    if (!empty($res)) {
		$res .= " and ";
	    }

	    if ($Dn < 2) {
		$res .= $ones[$Dn * 10 + $n];
	    } else {
		$res .= $tens[$Dn];

		if ($n) {
		    $res .= "-" . $ones[$n];
		}
	    }
	}

	if (empty($res)) {
	    $res = "zero";
	}

	return $res;
    }

    /**
     * @uses Payment comparison report between brela staff and respective bank
     */
    function reconciliation() {
	$param['payment'] = $this->total_payment();
	$param['param'] = '';
	//print_r($this->Payment_modal->get_payments_records());
	$total = 0;
	$records = $this->Payment_modal->get_payments_records();
	foreach ($records as $data) {
	    $total+=$data->this_total;
	}
	foreach ($records as $data) {
	    $param['param'].="['" . ucwords(str_replace('_', ' ', $data->method)) . "', " . $data->this_total * 100 / $total . "],";
	}
	$this->load->view('reconciliation', $param);
    }

    function total_payment() {

	return $this->Payment_modal->get_total_payments();
    }

    /**
     * @uses Provide custom report for payment status used for reconciliation
     */
    function custom_report() {
	/* (old implementation) */
	//$this->load_view('report/custom'); 

	/* new implementation with jqxwidget */
	$this->load_view('report/jqx_custom');
    }

    /**
     * @uses Get specific payment record status 
     */
    function get_payment_table_report() {
	$request = $this->input->get_post('cat_name');
	if (!empty($request)) {
	    $name = strtolower(str_replace(' ', '_', $request));
	    $data['data'] = $this->Payment_modal->get_report_by_method_name($name);
	    $data['method'] = $name;
	    $data['cat_name'] = $request;

	    $request_file = $this->input->get_post('file');
	    if (!empty($request_file)) {

		$system_root_file = dirname(__FILE__) . '/../../' . time() . '.pdf';
		//$html = __DIR__ . '../../views/report/pdf.php';
		ob_start();
		$this->load->view('report/pdf', $data);
		$file_content = ob_get_clean();
		$this->generate_file($system_root_file, $file_content);
		echo 'file generate to ' . $system_root_file;
	    } else {
		$this->load->view('report', $data);
	    }
	} else {
	    die("Sorry, something went wrong in getting the result");
	}
    }

    function online_back_request() {
	$this->load->library('lib');
	Simplify::$publicKey = 'sbpb_NzNmYzY3YmItNTQ4OC00ZTM5LWFmYjMtZmQyNjA0MWUwZGUw';
	Simplify::$privateKey = 'O0Z9TZms0E6hpr3rnxBrO1Yud9cuHuDpSVlo/nNnAqt5YFFQL0ODSXAOkNtXTToq';
	$token = $_POST['simplifyToken'];
	$payment = Simplify_Payment::createPayment(array(
		    'amount' => '1000',
		    'token' => $token,
		    'description' => 'prod description',
		    'currency' => 'USD'
	));
	$this->db->insert('pay', array('key' => '', 'secret' => json_encode($_REQUEST), 'content' => json_encode($payment)));

	print_r($payment);
	if ($payment->paymentStatus == 'APPROVED') {
	    echo "Payment approved\n";
	}
    }

    function simplify($amount = 100, $invoice = 'ABX8695BZ', $description = 'BRELA Payment') {
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	$this->load->library('lib');

	$token = $this->input->get_post('simplifyToken');
	if (isset($token) && !empty($token)) {
	    $this->load->library('lib');
	    Simplify::$publicKey = 'sbpb_Njg4YzgwNjgtZGU5OC00ODgwLWIzODAtMTBlMTY3NzgwZWEx';
	    Simplify::$privateKey = 'ZqEufo6MIH4eYh+ZyNN1pLKvomNwEEuAjmEzzcgR0Qp5YFFQL0ODSXAOkNtXTToq';
	    //$token = $_POST['simplifyToken'];
	    $payment = Simplify_Payment::createPayment(array(
			'amount' => '1000',
			'token' => $token,
			'description' => 'prod description',
			'currency' => 'USD'
	    ));
	    $this->db->insert('pay', array('key' => '', 'secret' => json_encode($_REQUEST), 'content' => json_encode($payment), 'header' => json_encode($_SERVER)));

	    print_r($payment);
	    if ($payment->paymentStatus == 'APPROVED') {
		echo "Payment approved\n";
	    }
	} else {
	    $this->load_view('online/simplify');
	}
    }

    function checkout() {
//// All methods return an Array by default or you can set the format to 'json' to get a JSON response.
//	Twocheckout::format('json');
	//validate form input 
	//check if the request require invoice for 
	$data = $this->payment_summary();
	$this->load_view('online/twocheckout', $data);
    }

    private function payment_summary() {
	$invoice = $this->input->get_post('invoice');
	$invoice != '' ?
			$app_data = $this->Payment_modal->fetch_payment_data_by_invoice($invoice) :
			die('Error occured. Try again later or write to us via info@ors.go.tz');
	$data['amount'] = $app_data->amount;
	$data['summary'] = $app_data->summary;
	$data['invoice'] = $invoice;
	return $data;
    }

    public function process_twocheckout() {

	$this->load->library('2checkout/checkout');

	define('ENV', 'production');
	if (ENV == 'SANDBOX') {
	    define('CO_PRIVATE_KEY', '90D14491-B7A9-444A-A4BE-71D453FE87BC');
	    define('CO_SELLER_ID', '901273428');
	} else {
	    define('CO_PRIVATE_KEY', 'BEC72024-E7C1-11E4-901D-5ECC3A5D4FFE');
	    define('CO_SELLER_ID', '102514285');
	}


	Twocheckout::privateKey(CO_PRIVATE_KEY);
	Twocheckout::sellerId(CO_SELLER_ID);

// Your username and password are required to make any Admin API call.
	Twocheckout::username('inetstz');
	Twocheckout::password('TabitaSwilla2011');

// If you want to turn off SSL verification (Please don't do this in your production environment)
	Twocheckout::verifySSL(false);  // this is set to true by default
// To use your sandbox account set sandbox to true
	Twocheckout::sandbox(FALSE);

	//try {
	//get invoice information including currency, amount,state, city,email
	// payment information etc
	$invoice = $this->input->get_post('invoice');
	$payment_info = $this->Payment_modal->get_booking($invoice);
	//$payment_info->currency;

	$client = $this->db->query("SELECT concat_ws(' ', c.firstname, c.lastname) as client_name, d.name as district_name, r.name as region_name, c.nationality, c.email, c.phone_number FROM client c JOIN district d ON c.district_id=d.district_id JOIN region r ON d.region_id=r.region_id WHERE client_id='" . $this->client_id . "'")->row();

	//update the amount in the database to match the required USD
	$amountUSD = round($payment_info->amount / EXCHANGE_RATE, 2);
	$service_charge = 0.05 * $amountUSD + 0.47;
	$total = $amountUSD + $service_charge;

	//save in the database
	$this->db->update('booking', array('usd_amount' => $total), array('invoice' => $invoice));

	$param = array(
	    "merchantOrderId" => $invoice,
	    "token" => $_POST['token'],
	    "currency" => 'USD',
	    "total" => $total,
	    "billingAddr" => array(
		"name" => $client->client_name,
		"addrLine1" => $client->district_name,
		"city" => $client->region_name,
		"state" => 'TZ',
		"zipCode" => '255',
		"country" => $client->nationality,
		"email" => $client->email,
		"phoneNumber" => $client->phone_number
	    ),
	);

	$charge = Twocheckout_Charge::auth($param);

	$this->db->insert('pay', array('key' => $_POST['token'], 'secret' => '2checkout', 'content' => json_encode($charge), 'header' => json_encode($_SERVER)));

	if ($charge['response']['responseCode'] == 'APPROVED') {
	    //if payment is successful
	    /*
	     * 1. return information to user to inform about that
	     * 2. generate receipt and send the receipt to user email as normal
	     * 3. approve user, there is no need for user to enter receipt number
	     */
	    $this->validate_bank_card_payment($charge);
	} else {
	    echo '<div class="alert alert-danger">' . $charge['response']['responseMsg'] . '</div>';
	}
	//} catch (Twocheckout_Error $e) {
	// print_r($e->getMessage());
	//}
    }

    function validate_bank_card_payment($charge) {

	//receipt generation

	$query = $this->db->query("select payment_id, control from receive_payment('{$charge['response']['merchantOrderId']}', "
			. "'', "
			. "'{$charge['response']['transactionId']}', "
			. "'', "
			. "{$charge['response']['total']}, "
			. "'BANK_CARD',"
			. "'',"
			. "'BANK_CARD',"
			. "'',"
			. "'{$charge['response']['currencyCode']}')")->row();
	//get receipt information

	if ($query->control == 1) {
	    //successful,
	    $receipt = $this->Payment_modal->get_receipt_info($query->payment_id);
	    //send SMS to users who have the invoice, and not the phone number used to make payment
	    $message = "RECEIPT : " . $receipt->code . " confirmed. " . $charge['response']['currencyCode'] . " " . $charge['response']['total'] . "/= received by BRELA for " . $charge['response']['billingAddr']['name'] . " on " . date('d/m/Y') . " at " . date('h:i:s A') . " INVOICE: " . $charge['response']['merchantOrderId'];
	    // $user->email
	    $user = $this->Payment_modal->get_client_info($receipt->client_id);

	    $this->send_email($receipt->client_id, $user->email, 'ORS Payment Receipt', $message, $user->firstname . ' ' . $user->lastname);
	    $this->send_sms($receipt->client_id, $user->phone_number, $message);
	    $this->send_pdf_receipt($query->payment_id);

	    echo '<div class="alert alert-success">Payment has been succesfful processed</div>';
	} else if ($query->control == 2) {
	    //payment is duplicated, already received
	    echo '<div class="alert alert-warning">Payment has already being processed</div>';
	} else if ($query->control == 3) {
	    // invoice comes with amount which is not valid
	    echo '<div class="alert alert-danger">Payment amount is not valid</div>';
	}
    }

    /**
     * 
     * @param Integer $amount ::Amount in USD that client needed to pay
     * @param String $description  :: Payment description to which a client needs to see
     */
    function wepay($amount = 100, $invoice = 'ABX8695BZ', $description = 'BRELA Payment') {

	/**
	 * Put your API credentials here:
	 * Get these from your API app details screen
	 * https://stage.wepay.com/app
	 */
	$client_id = "75212";
	$client_secret = "0f6a172d68";
	$access_token = "STAGE_9bd835a9a858a09c148f3b57b9d9ad0e9641c07dd0d668ec6d9b3b0a168e32c8";
	$account_id = "157182828"; // you can find your account ID via list_accounts.php which users the /account/find call

	/**
	 * Initialize the WePay SDK object 
	 */
	$this->load->helper('wepay');
	//require '../wepay.php';
	Wepay::useStaging($client_id, $client_secret);
	$wepay = new WePay($access_token);

	/**
	 * Make the API request to get the checkout_uri
	 * 
	 */
	try {
	    // print_r(curl_version()); exit;
	    $checkout = $wepay->request('/checkout/create', array(
		'account_id' => $account_id, // ID of the account that you want the money to go to
		'amount' => $amount, // dollar amount you want to charge the user
		'reference' => $invoice, //reference which unique identify certain payment
		'short_description' => $description, // a short description of what the payment is for
		'type' => "SERVICE", // the type of the payment - choose from GOODS SERVICE DONATION or PERSONAL
		'mode' => "iframe", // put iframe here if you want the checkout to be in an iframe, regular if you want the user to be sent to WePay
		    )
	    );
	    //print_r($checkout); exit;
	} catch (WePayException $e) { // if the API call returns an error, get the error message for display later
	    $error = $e->getMessage();
	}
	if (isset($error)) {
	    $body = '<h2 style="color:red">ERROR: ' . $error . ' </h2>';
	} else {
	    $body = '<div id="checkout_div"></div>
		
			<script type="text/javascript" src="https://stage.wepay.com/js/iframe.wepay.js">
			</script>
			
			<script type="text/javascript">
			WePay.iframe_checkout("checkout_div", "' . $checkout->checkout_uri . '");
			</script>';
	}
	echo $body;
    }

    /**
     * @todo: Provide IPN report for wepay service
     */
    protected function wepay_ipn() {
	
    }

    function service_charge() {
	$data['services'] = $this->db->query("select a.amount, a.description, a.finance_code_id, b.name as name, a.status, b.section_id, c.section_count from finance_code a join section b on a.section_code = b.section_code join (select section_code, count(finance_code_id) as section_count from finance_code group by section_code) as c on b.section_code = c.section_code order by b.section_id asc")->result();
	$data['service_count'] = $this->db->query("select count(section_id) as count from section")->row();
	$this->load->view('service_charge', $data);
    }

    function save_service_charge() {
	$pk = $this->input->get_post('pk');
	$value = $this->input->get_post('value');
	$this->db->query("update finance_code set amount = $value where finance_code_id = $pk");
    }

    function notify_issue() {
	$notify = $this->db->query("SELECT concat_ws(' ',c.firstname,c.lastname) as client_name, c.email, p.invoice, p.reg_date, p.name, p.amount, p.booking_id, p.payment_ready, 
       p.client_id
  FROM payment_requests p JOIN client c ON p.client_id=c.client_id where p.payment_ready='0' and p.booking_id is not null LIMIT 20")->result();
	foreach ($notify as $status) {
	    $message = 'Hello ' . $status->client_name . '.  Currently we are working on a problem encountered in CRDB payment channel to resolve it as soon as possible. We kindly advice you to do payment for ' . $status->name . '. via one of the following NMB branches; Kariakoo Branch, Bank House Branch, Temeke Branch, NMB House(TPA) Branch, Milimani city Branch and Clock City Branch (Arusha). Please specify your invoice number during payment and ensure you get a receipt code from BRELA. Incase of any problem, please contact us via +255 754 314 049 or +255 713 233 180';
	    //echo $message.'<br/><br/><br/><br/><br/>';

	    $this->send_email($status->client_id, $status->email, 'BRELA CRDB Payment Issue', $message);
	}
    }

}
