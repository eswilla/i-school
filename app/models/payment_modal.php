<?php

/**
 * Description of payment
 * 
 * @author Ephraim Swilla
 */
class Payment_modal extends CI_Model {

    /**
     * 
     */
    public function __construct() {
	parent::__construct();
	$this->load->database();
    }

    function get_payment_request() {
	$client_id = $this->session->userdata("client_id");
	$query = $this->db->query("SELECT count(*) as count FROM booking b JOIN bnm_app a on b.booking_id = b.booking_id  WHERE a.client_id='" . $client_id . "' ");
	return $query->result();
    }

    function fetch_payment_data($app_id) {
	$query = $this->db->query("SELECT  b.amount, b.invoice, b.booking_id ,a.name FROM booking b JOIN bnm_app a on a.booking_id = b.booking_id  WHERE a.bnm_app_id = '" . $app_id . "' LIMIT 1 ");
	$result = $query->result();
	return array_shift($result);
    }

    function fetch_payment_data_by_invoice($invoice) {
	$query = $this->db->query("SELECT  amount, invoice, booking_id, summary  FROM booking WHERE invoice = '" . $invoice . "' LIMIT 1 ");
	$result = $query->result();
	return array_shift($result);
    }

    /**
     * 
     * @param type $transaction_id
     * @return mixed
     */


    function validate($code,$invoice) {
	$query = $this->db->query("SELECT * from validate_receipt('".trim($invoice)."','" . trim($code) . "')")->row();
	return $query;
    }

    function get_api_user($api_key, $api_secret) {
	$query = $this->db->query("SELECT * FROM payment_api WHERE api_key='" . $api_key . "' AND api_secret='" . $api_secret . "' LIMIT 1 ");
	$result = $query->result();
	return array_shift($result);
    }

    function get_receipt_info($payment_id) {
	$query = $this->db->query("SELECT p.receipt_code AS code, r.receipt_id, p.name, p.booking_id, p.amount,p.summary,p.client_id FROM payment_requests p JOIN receipt r ON r.payment_id=p.payment_id WHERE p.payment_id = " . $payment_id . " limit 1");
	$result = $query->result();
	return array_shift($result);
    }

    function get_email_from_search($booking_id) {
	$query = $this->db->query("SELECT email FROM official_search WHERE booking_id='" . $booking_id . "' LIMIT 1 ");
	$result = $query->result();
	return array_shift($result);
    }

    function get_client_info($client_id) {
	$query = $this->db->query("SELECT email, firstname,lastname,phone_number FROM  client WHERE client_id='" . $client_id . "' ");
	$result = $query->result();
	return array_shift($result);
    }

    function get_booking($invoice_number) {
	$query = $this->db->query("SELECT
	b.invoice,
	b.booking_id,
	p.payment_id,
	b.currency,
	b.amount,
	r.name,
CASE WHEN P.PAYMENT_ID IS NULL THEN '0' ELSE '1' END AS PAYMENT_STATUS,
	RIGHT (f.section_code, 2) AS code
FROM
	booking b
JOIN SECTION f ON b.section_id = f.section_id 
LEFT JOIN payment p ON p.booking_id=b.booking_id
LEFT JOIN payment_requests r ON r.booking_id=b.booking_id
WHERE
	b.invoice = '" . $invoice_number . "'
LIMIT 1 ");
	$result = $query->result();
	return array_shift($result);
    }

    /**
     * 
     * @return mixed
     */
    function get_history() {
	$client_id = $this->session->userdata("client_id"); //match a session id of client who log in
	$query = $this->db->query("SELECT r.invoice, r.file_location, r.amount, r.payer, r.currency, r.book_date,r.receipt_date,r.receipt_code, p.payment_id FROM receipt_info r JOIN payment_requests p ON r.invoice=p.invoice WHERE r.client_id='" . $client_id . "' ");
	return $query->result();
    }

    function search_receipt_id($receipt) {
	$query = $this->db->query("SELECT * FROM receipt WHERE code='" . $receipt . "' ");
	return $query->result();
    }

    function save_invoice($data) {
	return $this->db->insert('booking', $data);
    }

    function get_receipt_from_booking($invoice) {
	$query = $this->db->query("SELECT code FROM receipt WHERE payment_id=(select payment_id FROM payment where booking_id=(select booking_id from booking where invoice='" . $invoice . "') ) LIMIT 1");
	$result = $query->result();
	return array_shift($result);
    }

    function get_payments_records() {
	$query = $this->db->query("select a.method, count(a.payment_id) as this_total, (count(a.payment_id)/b.total) * 100 as percentage from payment a cross join (select count(*) as total from payment) as b group by a.method, b.total");
	return $query->result();
    }

    function get_report_by_method_name($name = '') {
	$query = $this->db->query("select p.date,p.bank_name,p.bank_account_number,p.method,p.amount,p.bank_transaction_id,p.mobile_transaction_id,p.status,p.currency,  r.invoice,r.receipt_code as code, r.name , concat_ws(' ', c.firstname, c.middle_name, c.lastname) as client_name  from payment p  join payment_requests r ON r.booking_id=p.booking_id  JOIN client_info c ON c.client_id=r.client_id where lower(p.method) like '%" . $name . "%' ");
	return $query->result();
    }

    function get_payment_list($client_id) {
	$query = $this->db->query("SELECT * FROM bnm_app_status WHERE client_id ='" . $client_id . "' AND status='Approved' AND is_pay_confirmed='0'");
	return $query->result();
    }

    function get_total_payments() {
	$query['mobile'] = $this->db->query("SELECT SUM(amount) as total FROM payment WHERE lower(method) ='mobile' AND date=now() ")->row();
	$query['crdb'] = $this->db->query("SELECT SUM(amount) as total FROM payment WHERE lower(bank_name) ='CRDB' AND date=now() ")->row();
	$query['nmb'] = $this->db->query("SELECT SUM(amount) as total FROM payment WHERE lower(bank_name) ='NMB' AND date=now() ")->row();
	$query['cards'] = $this->db->query("SELECT SUM(amount) as total FROM payment WHERE lower(method) ='CARD' AND date=now() ")->row();
	return $query;
    }

    function get_business_by_payment_id($payment_id) {
	
    }

}
