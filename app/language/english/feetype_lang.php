<?php

/* List Language  */
$lang['panel_title'] = "Fee Type";
$lang['add_title'] = "Add a Fee Type";
$lang['slno'] = "#";
$lang['feetype_name'] = "Fee Type";
$lang['feetype_amount'] = "Amount";
$lang['feetype_is_repeative'] = "Is Repeative";
$lang['feetype_is_repeative_yes'] = "Yes";
$lang['feetype_is_repeative_no'] = "No";
$lang['feetype_startdate'] = "Start Date";
$lang['feetype_enddate'] = "End Date";
$lang['feetype_note'] = "Note";
$lang['action'] = "Action";

$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */
$lang['add_feetype'] = 'Add Fee';
$lang['update_feetype'] = 'Update Fee Type';