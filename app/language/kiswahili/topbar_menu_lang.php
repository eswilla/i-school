<?php

/* List Language  */
$lang['language'] = "Luga";
$lang['profile'] = "Ukurasa";
$lang['change_password'] = "Badili Nywila";
$lang['panel_title'] = "Nywila";
$lang['old_password'] = "Nywila ya Zamani";
$lang['new_password'] = "Nywila Mpya";
$lang['re_password'] = "Ingiza Nywila";
$lang['logout'] = "Logua";
$lang['view_more'] = "Angalia Taarifa Zote";
$lang['la_fs'] = "Una";
$lang['la_ls'] = "taarifa";
$lang['close'] = "Funga";
$lang['username']="Jina Tumizi";
$lang['login_hint']="Karibu na ingia ndani";

$lang['Admin'] = "Admini";
$lang['Student'] = "Mwanafunzi";
$lang['Parent'] = "Wazazi";
$lang['Teacher'] = "Mwalimu";
$lang['Accountant'] = "Mhasibu";
$lang['Librarian'] = "Mkutubi";

$lang['success'] = "Success!";
$lang['cmessage'] = "Nywila yako imebadilishwa.";

/* Menu List */
$lang['menu_add'] = 'Ongeza';
$lang['menu_edit'] = 'Badili';
$lang['menu_view'] = 'Angalia';
$lang['menu_success'] = 'Success';
$lang['menu_dashboard'] = 'Dashibodi';
$lang['menu_student'] = 'Wanafunzi';
$lang['menu_parent'] = 'Wazazi';
$lang['menu_teacher'] = 'Walimu';
$lang['menu_user'] = 'Watu';
$lang['menu_classes'] = 'Madarasa';
$lang['menu_section'] = 'Mikondo';
$lang['menu_subject'] = 'Masomo';
$lang['menu_grade'] = 'Madaraja';
$lang['menu_exam'] = 'Mtihani';
$lang['menu_examschedule'] = 'Ratiba ya Mtihani';
$lang['menu_library'] = 'Maktaba';
$lang['menu_mark'] = 'Mark';
$lang['menu_routine'] = 'Routine';
$lang['menu_attendance'] = 'Attendance';
$lang['menu_member'] = 'Member';
$lang['menu_books'] = 'Books';
$lang['menu_issue'] = 'Issue';
$lang['menu_fine'] = 'Fine';
$lang['menu_profile'] = 'Profile';
$lang['menu_transport'] = 'Transport';
$lang['menu_hostel'] = 'Hostel';
$lang['menu_category'] = 'Category';
$lang['menu_account'] = 'Account';
$lang['menu_feetype'] = 'Fee Type';
$lang['menu_setfee'] = 'Set Fee';
$lang['menu_balance'] = 'Balance';
$lang['menu_paymentsettings'] = 'Payment Settings';
$lang['menu_expense'] = 'Expense';
$lang['menu_notice'] = 'Notice';
$lang['menu_report'] = 'Report';
$lang['menu_setting'] = 'Setting';

$lang['accountant'] = 'Accountant';
$lang['librarian'] = 'Librarian';

$lang['upload'] = "Upload";


/* Start Update Menu */
$lang['menu_sattendance'] = 'Student Attendance';
$lang['menu_tattendance'] = 'Teacher Attendance';
$lang['menu_eattendance'] = 'Exam Attendance';
$lang['menu_promotion'] = 'Promotion';
$lang['menu_media'] = 'Media';
$lang['menu_message'] = 'Message';
$lang['menu_smssettings'] = 'SMS Settings';
$lang['menu_mailandsms'] = 'Mail / SMS';
$lang['menu_mailandsmstemplate'] = 'Mail / SMS Template';
$lang['menu_invoice'] = 'Invoice';

/* End Update Menu */
